FROM ubuntu:22.04

RUN apt -y update && \
    echo "deb http://build.openvpn.net/debian/openvpn/release/2.5 bionic main" > /etc/apt/sources.list.d/openvpn-aptrepo.list && \
    apt install -y openvpn easy-rsa && \
    rm -rf /tmp/ && \
    rm -rf /var/lib/apt/lists/*

# Configure ovpn
RUN cp -R /usr/share/easy-rsa /etc/openvpn/

ENV EASY_RSA="/etc/openvpn/easy-rsa/"
ENV OPENSSL="openssl"
ENV PKCS11TOOL="pkcs11-tool"
ENV GREP="grep"
ENV KEY_CONFIG="/etc/openvpn/easy-rsa/"
ENV KEY_DIR="$EASY_RSA/keys/"
ENV PKCS11_MODULE_PATH="dummy"
ENV PKCS11_PIN="dummy"
ENV KEY_SIZE=2048
ENV CA_EXPIRE=3650
ENV KEY_EXPIRE=3650
ENV KEY_COUNTRY="COUNTRY"
ENV KEY_PROVINCE="PROVINCE"
ENV KEY_CITY="CITY"
ENV KEY_ORG="ORG"
ENV KEY_EMAIL="EMAI@ORG.local"
ENV KEY_OU="ORG"
ENV KEY_NAME="ovpn_server"

EXPOSE 1194/tcp

ENTRYPOINT [ "tail" ]
CMD [ "-f", "/dev/null" ]
